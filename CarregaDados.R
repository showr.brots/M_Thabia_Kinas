######################################################################
### Carrega TabelaDeCampo
######################################################################

# Carrega pacote de leitura Excel
library(readxl)

# Le tabela Excel com os TabelaDeCampo
TabelaDeCampo <- read_excel(path = "TabelaDeCampo.xlsx", 
                            col_types = c("text",     # Local
                                          "numeric",  # LocalNum
                                          "text",     # Individuo
                                          "text",     # Especie
                                          "numeric",  # EspecieNum  
                                          "date",     # PeriodoColeta
                                          "numeric",  # EstacaoNum
                                          "numeric",  # TotalFolhas
                                          "numeric",  # FolhasEstereis
                                          "numeric",  # FerteisMaduras
                                          "numeric",  # FerteisVerdes
                                          "numeric",  # Baculos
                                          "numeric",  # FolhasMortas
                                          "numeric",  # FournierEstereis
                                          "numeric",  # FournierFerteis
                                          "numeric",  # FournierVerdes
                                          "numeric",  # FournierBaculos
                                          "numeric",  # FournierSenescencia
                                          "numeric",  # FrequenciaEstereis
                                          "numeric",  # FrequenciaFerteis
                                          "numeric",  # FrequenciaVerdes
                                          "numeric",  # FrequenciaBaculos
                                          "numeric",  # FrequenciaSenescencia
                                          "numeric",  # TempEstaCalc
                                          "numeric",  # FotoEstaCalc
                                          "numeric",  # PreciEstaCalc
                                          "numeric",  # TemperaturaLocal
                                          "numeric",  # UmidadeAr
                                          "numeric",  # Pluviometro
                                          "numeric",  # PAR
                                          "numeric",  # TempEstaCalcZ
                                          "numeric",  # FotoEstaCalcZ
                                          "numeric",  # PreciEstaCalcZ
                                          "numeric",  # TemperaturaLocalZ
                                          "numeric",  # UmidadeArZ
                                          "numeric",  # PluviometroZ
                                          "numeric")) # PARZ 

# Descarrega pacote de leitura Excel
detach(package:readxl)

# Ajusta o tipo de objeto
TabelaDeCampo$Local                 <- as.factor(Local)
TabelaDeCampo$LocalNum              <- as.factor(LocalNum)
TabelaDeCampo$Individuo             <- as.factor(Individuo)
TabelaDeCampo$Especie               <- as.factor(Especie)
TabelaDeCampo$EspecieNum            <- as.factor(EspecieNum)
TabelaDeCampo$EstacaoNum            <- as.factor(EstacaoNum)

TabelaDeCampo$PeriodoColeta         <- as.Date(PeriodoColeta)

TabelaDeCampo$FolhasEstereis        <- as.integer(FolhasEstereis)
TabelaDeCampo$FerteisMaduras        <- as.integer(FerteisMaduras)
TabelaDeCampo$FerteisVerdes         <- as.integer(FerteisVerdes)
TabelaDeCampo$Baculos               <- as.integer(Baculos)
TabelaDeCampo$FolhasMortas          <- as.integer(FolhasMortas)
TabelaDeCampo$TotalFolhas           <- as.integer(TotalFolhas)

TabelaDeCampo$FournierEstereis      <- as.factor(FournierEstereis)
TabelaDeCampo$FournierFerteis       <- as.factor(FournierFerteis)
TabelaDeCampo$FournierVerdes        <- as.factor(FournierVerdes)
TabelaDeCampo$FournierBaculos       <- as.factor(FournierBaculos)
TabelaDeCampo$FournierSenescencia   <- as.factor(FournierSenescencia)

TabelaDeCampo$FrequenciaEstereis    <- as.logical(FrequenciaEstereis)
TabelaDeCampo$FrequenciaFerteis     <- as.logical(FrequenciaFerteis)
TabelaDeCampo$FrequenciaVerdes      <- as.logical(FrequenciaVerdes)
TabelaDeCampo$FrequenciaBaculos     <- as.logical(FrequenciaBaculos)
TabelaDeCampo$FrequenciaSenescencia <- as.logical(FrequenciaSenescencia)

TabelaDeCampo$TempEstaCalc          <- as.double(TempEstaCalc)
TabelaDeCampo$FotoEstaCalc          <- as.double(FotoEstaCalc)
TabelaDeCampo$PreciEstaCalc         <- as.double(PreciEstaCalc)
TabelaDeCampo$TemperaturaLocal      <- as.double(TemperaturaLocal)
TabelaDeCampo$UmidadeAr             <- as.double(UmidadeAr)
TabelaDeCampo$Pluviometro           <- as.double(Pluviometro)
TabelaDeCampo$PAR                   <- as.double(PAR)
TabelaDeCampo$TempEstaCalcZ         <- as.double(TempEstaCalcZ)
TabelaDeCampo$FotoEstaCalcZ         <- as.double(FotoEstaCalcZ)
TabelaDeCampo$PreciEstaCalcZ        <- as.double(PreciEstaCalcZ)
TabelaDeCampo$TemperaturaLocalZ     <- as.double(TemperaturaLocalZ)
TabelaDeCampo$UmidadeArZ            <- as.double(UmidadeArZ)
TabelaDeCampo$PluviometroZ          <- as.double(PluviometroZ)
TabelaDeCampo$PARZ                  <- as.double(PARZ)

#####
# Limpa excedentes de memoria
gc()

#####
# Salva Objeto
save(TabelaDeCampo, file = "./TabelaDeCampo.RData", 
     ascii = TRUE, 
     compress = TRUE, 
     compression_level = 9)

#####
## Fim
#
