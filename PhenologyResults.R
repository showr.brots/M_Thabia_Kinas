############################################################
### Resultados do modelo
############################################################

# Fórmula
modelo$Fixed$formula

# Sumariza modelo
summaryModel <- summary(modelo)

resultados <- tbl_df(summaryModel$solutions)
resultados$Variavel <- rownames(summaryModel$solutions)
resultados <- resultados[-1,]

resultados$ValorEvidencia <- if_else((1 - resultados$pMCMC) > resultados$pMCMC,
                                     (1 - resultados$pMCMC) / resultados$pMCMC,
                                     resultados$pMCMC / (1 - resultados$pMCMC))

resultados$Evidencia <- if_else(resultados$ValorEvidencia <= 3.20, "Fraca",
                                if_else(resultados$ValorEvidencia <= 10.00, "Substancial",
                                        if_else(resultados$ValorEvidencia <= 100, "Forte", "Decisiva")))
                                        
resultados$Unidade <-  if_else(resultados$Variavel == "UmidadeArZ", 
                         (mean(TabelaDeCampo$UmidadeAr) + (resultados$post.mean * 
                                                             sd(TabelaDeCampo$UmidadeAr))),
                       if_else(resultados$Variavel == "PluviometroZ", 
                         (mean(TabelaDeCampo$Pluviometro) + (resultados$post.mean * 
                                                               sd(TabelaDeCampo$Pluviometro))),
                       if_else(resultados$Variavel == "TemperaturaLocalZ", 
                         (mean(TabelaDeCampo$TemperaturaLocal) + (resultados$post.mean * 
                                                                    sd(TabelaDeCampo$TemperaturaLocal))),
                       if_else(resultados$Variavel == "PARZ", 
                         (mean(TabelaDeCampo$PAR) + (resultados$post.mean * 
                                                       sd(TabelaDeCampo$PAR))),
                       if_else(resultados$Variavel == "TempEstaCalcZ", 
                         (mean(TabelaDeCampo$TempEstaCalc) + (resultados$post.mean * 
                                                               sd(TabelaDeCampo$TempEstaCalc))),
                       if_else(resultados$Variavel == "PreciEstaCalcZ", 
                         (mean(TabelaDeCampo$PreciEstaCalc) + (resultados$post.mean * 
                                                                sd(TabelaDeCampo$PreciEstaCalc))),
                       if_else(resultados$Variavel == "FotoEstaCalcZ", 
                          (mean(TabelaDeCampo$FotoEstaCalc) + (resultados$post.mean * 
                                                                 sd(TabelaDeCampo$FotoEstaCalc))),
                          0)))))))

resultados <- resultados[,c("Variavel", 
                            "post.mean",
                            "l-95% CI",
                            "u-95% CI",
                            "eff.samp",
                            "pMCMC",
                            "ValorEvidencia",
                            "Evidencia",
                            "Unidade")]

parecer <- BayesianGLM.R2()

# Calcula Nagelkerke's R quadrado.
#NagelkerkeR2(modelo)$R2

# # Tratamento Exceção
# tryCatch({
#   # Plotting estimates of generalized linear models
#   sjp.glm(modelo)
# }, warning = function(w) {
# }, error = function(e) {
# }, finally = {
# })
# 
# # Predicted probabilities or incidents
# sjp.glm(modelo, type = "slope", facet.grid = TRUE, show.ci = TRUE)
# 
# 
# # Tratamento Exceção
# tryCatch({
#   # Variance Inflation Factors
#   sjp.glm(modelo, type = "vif")
# }, warning = function(w) {
# }, error = function(e) {
# }, finally = {
# })
# 
# # Tratamento Exceção
# tryCatch({
#   # Marginal effects
#   sjp.glm(modelo, type = "eff")
# }, warning = function(w) {
# }, error = function(e) {
# }, finally = {
# })
# 
# # Tratamento Exceção
# tryCatch({
#   # plot all marginal effects, as grid, proper x-axes
#   # also, set margins for this example
#   plot_grid(sjp.glm(modelo, 
#                     type = "eff", 
#                     facet.grid = FALSE, 
#                     show.ci = TRUE, 
#                     prnt.plot = FALSE)$plot.list, 
#             margin = c(0.3, 0.3, 0.3, 0.3))
# }, warning = function(w) {
# }, error = function(e) {
# }, finally = {
# })

####
## Fim
#
