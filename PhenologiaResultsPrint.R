############################################################
### Resultados do modelo
############################################################


kable(resultados, digits = 5)

cat(paste0("\n|**$R^2$**|**Deviance**|**DIC**|\n"))
cat(paste0("|---------|------------|-------|\n"))
cat(paste0("|",  parecer$R2, "|", min(modelo$Deviance), "|",  modelo$DIC, "|\n"))


#####
## Fim
#
