---
title: "Fenologia de três espécies de samambaias epifíticas em borda artificial e interior de floresta com araucária Rio Grande do Sul, Brasil"
subtitle: "Estatísticas de Efeito de Borda "
author: 
- "Thábia O. H. Padoin"
- "João B. G. Brito"
- "Paul G. Kinas"
output: html_document
---

## **[Modelo de Comparação entre borda e interior](http://www.thabia.esy.es/ModeloComparacaoLocais)**

## **[Modelo de Fenologia por Espécies](http://www.thabia.esy.es/ModeloFenologiaEspecies)**

## **[Modelo de Fenologia por Espécies e Locais](http://www.thabia.esy.es/ModeloFenologiaEspeciesLocais)**


